# Configmap Generation using Kustomization

The use of the `generatorOptions` allows top set a generated suffix of the ConfigMap and its content.

```yaml
generatorOptions:
  disableNameSuffixHash: false
```

## On Changes

When changing content within the files for the Configmap, see file `contents.txt`, the kustomization generates a ConfigMap with a new hash-suffix.
Furthermore, the `readinessProbe` bases of the availability of the file `contents.txt` within the container.
Hence, when changing the included files for the configmap, we can also simulate a _non-ready_ Pod.

With the rolling update strategy set to ...

```yaml
strategy:
    rollingUpdate:
      maxSurge: 0
      maxUnavailable: 1
```

... we are able to tear down one of the existing pods and create a new one with a new _ReplicaSet-ID_.
To see the process in the flow, the readinessProbe waits several seconds to signal the readiness signal to K8s.

### Initial Deploy

```bash
kubectl apply -k configmap-kustomization
```

```bash
configmap/my-configmap-62487t4456 created
deployment.apps/busybox created
```

... Verify the deployment:

```bash
k get pods -n default
```

```bash
NAME                       READY   STATUS    RESTARTS   AGE
busybox-6f67c77d64-8p6d7   1/1     Running   0          27s
busybox-6f67c77d64-9k276   1/1     Running   0          27s
busybox-6f67c77d64-cblwr   1/1     Running   0          27s
busybox-6f67c77d64-htpcx   1/1     Running   0          27s
busybox-6f67c77d64-j5x99   1/1     Running   0          27s
busybox-6f67c77d64-njv8j   1/1     Running   0          27s
busybox-6f67c77d64-pcpkl   1/1     Running   0          27s
busybox-6f67c77d64-q6rnx   1/1     Running   0          27s
busybox-6f67c77d64-wvlnd   1/1     Running   0          27s
busybox-6f67c77d64-xm7kw   1/1     Running   0          27s
```

### Logs

The logs also show the content of the given ConfigMap and its respective mounted key to the folder.

```bash
1: Tue Feb 21 22:28:10 UTC 2023
Hello K8s
2: Tue Feb 21 22:28:11 UTC 2023
Hello K8s
3: Tue Feb 21 22:28:12 UTC 2023
Hello K8s
4: Tue Feb 21 22:28:13 UTC 2023
Hello K8s
5: Tue Feb 21 22:28:14 UTC 2023
Hello K8s
...
```

### Change Content

When changing the example configuraion file `kay.txt` and apply the kustomization again, the Pods are performing an Update based on their rolling Update Strategy. Hence, One-By-One.

1. Current Content of `contents.txt`: `Hello K8s`.
1. Updated Content of `contents.txt`: `Hello World`.

When applying the changes via kustomization:

```bash
kubectl apply -k configmap-kustomization
```

```bash
configmap/my-configmap-fc8t82gb6c created
deployment.apps/busybox configured
```

Seconds after the apply, we can see the changes for the deployment:

```bash
NAME                       READY   STATUS        RESTARTS   AGE
busybox-5d9845fb9-jb2xp    0/1     Running       0          7s
busybox-5d9845fb9-r24zr    1/1     Running       0          17s
busybox-6f67c77d64-8p6d7   1/1     Running       0          2m10s
busybox-6f67c77d64-9k276   1/1     Running       0          2m10s
busybox-6f67c77d64-cblwr   1/1     Running       0          2m10s
busybox-6f67c77d64-htpcx   1/1     Terminating   0          2m10s
busybox-6f67c77d64-j5x99   1/1     Running       0          2m10s
busybox-6f67c77d64-njv8j   1/1     Running       0          2m10s
busybox-6f67c77d64-pcpkl   1/1     Running       0          2m10s
busybox-6f67c77d64-q6rnx   1/1     Running       0          2m10s
busybox-6f67c77d64-wvlnd   1/1     Terminating   0          2m10s
busybox-6f67c77d64-xm7kw   1/1     Running       0          2m10s
```

_Note: See the new ReplicaSet-ID: `5d9845fb9`. Furthermore, it is important to see that only **1** Pod is in Non-Ready State._

The logs from a newly created Pod now show the changed content:

```bash
k logs -n default busybox-5d9845fb9-r24zr
```

```bash
1: Tue Feb 21 22:30:00 UTC 2023
Hello World
2: Tue Feb 21 22:30:01 UTC 2023
Hello World
3: Tue Feb 21 22:30:02 UTC 2023
Hello World
4: Tue Feb 21 22:30:03 UTC 2023
Hello World
5: Tue Feb 21 22:30:04 UTC 2023
Hello World

```

However, after a while, all Pods have been exchanged with the newly created ReplicaSet and the new content of the ConfigMap.

```bash
k get pods -n default
```

```bash
NAME                       READY   STATUS        RESTARTS   AGE
busybox-5d9845fb9-4xxzc    1/1     Running       0          83s
busybox-5d9845fb9-68572    1/1     Running       0          52s
busybox-5d9845fb9-74s45    1/1     Running       0          41s
busybox-5d9845fb9-bc5rn    1/1     Running       0          73s
busybox-5d9845fb9-hxzhv    1/1     Running       0          62s
busybox-5d9845fb9-jb2xp    1/1     Running       0          2m1s
busybox-5d9845fb9-kh46l    1/1     Running       0          110s
busybox-5d9845fb9-nrfpr    1/1     Running       0          99s
busybox-5d9845fb9-prg8s    1/1     Running       0          30s
busybox-5d9845fb9-r24zr    1/1     Running       0          2m11s
```

### Don't mess up the System when a ConfigChange is wrong

Even more critical is the use of the rolling update strategy when it comes to wrong configurations.
We can simulate this with the readiness probe which also checks whether the file is accessible or not.

```yaml
readinessProbe:
  exec:
    command: ["/bin/sh", "-c", "--", "cat /mnt/contents.txt"]
  initialDelaySeconds: 5
  periodSeconds: 5
```

Here we wanted a proper exit code from the call of `cat` on our `contents.txt` file.
In case the file does not exist or is unaccessible, the readiness probe fails.

We can now simulate a wrong configuration within the kustomization by changing the file's content of the ConfigMap to `wrong-contents.txt` which will now be mounted to the pods.

```yaml
configMapGenerator:
  - name: my-configmap
    files:
      - wrong-contents.txt
```

The deployment is valid and therefore a new ReplicaSet will be created when applying this to the cluster:

```kubectl apply -k configmap-kustomization
configmap/my-configmap-c7974fft67 created
deployment.apps/busybox configured
```

However, the new ReplicaSet has a problem already with the first Pod:

```bash
k get pods -n default
```

```bash
NAME                       READY   STATUS        RESTARTS   AGE
busybox-5d9845fb9-4xxzc    1/1     Running       0          3m23s
busybox-5d9845fb9-68572    1/1     Running       0          2m52s
busybox-5d9845fb9-74s45    1/1     Running       0          2m41s
busybox-5d9845fb9-bc5rn    1/1     Running       0          3m13s
busybox-5d9845fb9-hxzhv    1/1     Running       0          3m2s
busybox-5d9845fb9-jb2xp    1/1     Running       0          4m1s
busybox-5d9845fb9-kh46l    1/1     Running       0          3m50s
busybox-5d9845fb9-nrfpr    1/1     Running       0          3m39s
busybox-5d9845fb9-prg8s    1/1     Terminating   0          2m30s
busybox-5d9845fb9-r24zr    1/1     Running       0          4m11s
busybox-6bcbd58955-vvckx   0/1     Running       0          15s
```

The pod `busybox-6bcbd58955-vvckx` will never become Ready with its 1/1.
That's because of the missing file in the mount and the respective probe configuration.

When having a closer look:

```bash
k logs -n default busybox-6bcbd58955-vvckx
```

```bash
1: Tue Feb 21 22:33:56 UTC 2023
cat: can't open '/mnt/contents.txt': No such file or directory
...
```

The same we see when describing the Pod and see the readiness probe failing:

```bash
k describe pod -n default busybox-6bcbd58955-vvckx
```

```bash
...
    Readiness:    exec [/bin/sh -c -- cat /mnt/contents.txt] delay=5s timeout=1s period=5s #success=1 #failure=3
...
...
...
Events:
  Type     Reason     Age                    From               Message
  ----     ------     ----                   ----               -------
  ...
  Warning  Unhealthy  106s (x22 over 3m26s)  kubelet            Readiness probe failed: cat: can't open '/mnt/contents.txt': No such file or directory
```

The overall deployment with the faulty changes will not affect the whole Deployment and we still have the former Deployment with its proper working ReplicaSet available.
We only suffer 1 unready Pod for now.
The overall Application/Service availability shall then not be affected.

## Summary

Whenever changes happend to files which are mounted as content to a ConfigMap, a new ReplicaSet will be created.
This will happen due to the _suffix_ creation of the ConfigMap-Resource.
With the use of `kustomization` we are able to steer the update process.
New Pods will be created with the updated ConfigMaps.
Whenever changes are not generate a successful Readiness-State, the rolling update does not continue.
